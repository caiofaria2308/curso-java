import java.util.Locale;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		// exemplo1(sc);
		// exemplo2(sc);
		// exemplo3(sc);
		// exemplo4(sc);
		// exemplo5(sc);
		// exemplo6(sc);
		exemplo7(sc);
		sc.close();

	}

	public static void exemplo1(Scanner sc) {
		System.out.println("Exemplo 1 , Digite texto");
		String x;

		x = sc.next();

		System.out.println("Você digitou: " + x);
	}

	public static void exemplo2(Scanner sc) {
		System.out.println("Exemplo 2, Digite inteiro");
		int x;

		x = sc.nextInt();

		System.out.println("Você digitou: " + x);
	}

	public static void exemplo3(Scanner sc) {
		System.out.println("Exemplo 3, Digite double");
		double x;

		x = sc.nextDouble();

		System.out.println("Você digitou: " + x);
	}

	public static void exemplo4(Scanner sc) {
		System.out.println("Exemplo 4, Digite texto");
		char x;

		x = sc.next().charAt(0);

		System.out.println("Você digitou: " + x);
	}

	public static void exemplo5(Scanner sc) {
		System.out.println("Exemplo 5, Digite texto ,int e depois double");
		String x;
		int y;
		double z;

		x = sc.next();
		y = sc.nextInt();
		z = sc.nextDouble();

		System.out.println("Dados digitados:");
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}

	public static void exemplo6(Scanner sc) {
		System.out.println("Exemplo 6, Texto até quebra de linha");
		String s1, s2, s3;

		s1 = sc.nextLine();
		s2 = sc.nextLine();
		s3 = sc.nextLine();

		System.out.println("Dados digitados: ");
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

	}
	
	public static void exemplo7(Scanner sc) {
		System.out.println("Exemplo 7, Inteiro e depois Texto até quebra de linha");
		int x;
		String s1, s2, s3;
		
		x = sc.nextInt();
		// Quando se usa qualquer comando diferente de nextLine antes do próprio nextLine
		// a quebra de linha vai ser armazenada na próxima variável
		// no caso desse exemplo, ao preencher o valor de x e apertar enter
		// a quebra de linha ao preencher esse valor preencherá também a variável s1
		// deixando a mesma vazia
		// uma forma de corrigir, é colocar um nextLine() antes do que vai ser funcional
		sc.nextLine();
		s1 = sc.nextLine();
		s2 = sc.nextLine();
		s3 = sc.nextLine();

		System.out.println("\nDados digitados: ");
		System.out.println(x);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		
	}

}
