package entities;

public class BusinessAccount extends Account {
	private Double loanLimit;

	public BusinessAccount() {
		super();
	}

	/**
	 * @param number
	 * @param holder
	 * @param balance
	 * @param loanLimit
	 */
	public BusinessAccount(Integer number, String holder, Double balance, Double loanLimit) {
		super(number, holder, balance);
		this.loanLimit = loanLimit;
	}

	/**
	 * @return the loanLimit
	 */
	public Double getLoanLimit() {
		return loanLimit;
	}

	/**
	 * @param loanLimit the loanLimit to set
	 */
	public void setLoanLimit(Double loanLimit) {
		this.loanLimit = loanLimit;
	}
	
	/*
	 * @param amount to deposit loan
	 */
	public void loan(double amount) {
		if (amount <= this.loanLimit) {
			this.balance += amount - 10;
		}
	}

}
