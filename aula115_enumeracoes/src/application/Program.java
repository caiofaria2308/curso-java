package application;

import java.util.Date;

import entities.Order;
import entities.enums.OrderStatus;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Enumeração é um tipo especial que serve para especificar
		 * de forma literal um conjunto de constantes relacionadas
		 */
		Order order = new Order(1080, new Date(), OrderStatus.PROCESSING);
		System.out.println(order);
		OrderStatus os1 = OrderStatus.DELIVERED;
		OrderStatus os2 = OrderStatus.valueOf("DELIVERED");
		System.out.println(os1 + " " + os2);
	}

}
