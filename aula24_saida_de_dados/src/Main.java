import java.util.Locale;

public class Main {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		System.out.print("Ola world!");
		System.out.println(" Buenos días!");
		byte idade = 21;
		System.out.println(idade);
		double altura = 1.8345;
		System.out.println(altura);
		// print formatando a altura, %n é a quebra de linha
		System.out.printf("%.2f%n", altura);
		//Contatenando 
		System.out.println("Sua idade: " + idade + " Sua altura: " + altura);
		System.out.printf("Sua idade: %d Sua altura: %.2f %n", idade, altura);
		//Exercicio
		exercicio_fixacao_1();
		
	}
	
	public static void exercicio_fixacao_1() {
		String product1 = "Computer";
		String product2 = "Office desk";
		
		int age = 30;
		int code = 5290;
		char gender = 'F';
		
		double price1 = 2100.00;
		double price2 = 650.50;
		double measure = 53.234567;
		
		System.out.printf("Products:%n"
				+ "%s, which price is R$%.2f%n"
				+ "%s, which price is R$%.2f%n%n"
				+ "Record: %d years old, code %d and gender: %s %n%n"
				+ "Measue with eight decimal places: R$%.8f %n"
				+ "Rouded (three decimal places): R$%.3f %n", 
				product1,
				price1,
				product2,
				price2,
				age,
				code,
				gender,
				measure,
				measure
		);
	}

}
