package application;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 20;
		// boxing
		Object obj = x;
		//unboxing
		int y = (int) obj;
		System.out.println(obj + " " + y);
		// Wrapper class -> boxing/ unboxing de forma natural
		Integer obj2 = x;
		int z = obj2 * 2;
		System.out.println(obj + " " + z);
		/*
		 * Wrapper class é muito utilizado para classes, já que wrappers class pode receber
		 * valor nulo
		 * ex:
		 * public class Product{
		 * 	public String Nome;
		 * 	public Integer idade;
		 * }
		 */

	}

}
