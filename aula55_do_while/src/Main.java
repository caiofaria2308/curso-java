import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		example1(sc);
		sc.close();

	}

	private static void example1(Scanner sc) {
		// TODO Auto-generated method stub
		int soma = 0;
		char repetir = 'S';
		do {
			System.out.printf("Digite um número inteiro: ");
			soma += sc.nextInt();

			System.out.printf("\nDigite S para repetir e N para parar: ");
			repetir = sc.next().charAt(0);

		} while (repetir == 'S' || repetir == 's');
		System.out.println(soma);

	}

}
