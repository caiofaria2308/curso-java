package application;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] vect = new String[] { "Maria", "Bob", "Alex" };
		example1_modo_anterior(vect);
		example2_foreach(vect);
	}

	private static void example1_modo_anterior(String[] vect) {
		// TODO Auto-generated method stub
		for (int i = 0; i < vect.length; i++) {
			System.out.println(vect[i]);
		}

	}

	private static void example2_foreach(String[] vect) {
		// TODO Auto-generated method stub
		System.out.println("----------__FOREACH__----------");
		for (String obj : vect) {
			System.out.println(obj);
		}

	}

}
