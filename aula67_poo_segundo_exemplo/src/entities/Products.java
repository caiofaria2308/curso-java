package entities;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Products {
	public int id = 0;
	public Map<String, Product> list = new HashMap<String, Product>();

	public void newProduct(Scanner sc) {
		Product p = new Product();
		System.out.println("Enter the product data. ");
		System.out.printf("Name: ");
		id += 1;
		p.id = id;
		p.name = sc.next();
		System.out.printf("Price: ");
		p.price = sc.nextDouble();
		System.out.printf("Quantity in stock: ");
		p.quantity = sc.nextInt();
		showProduct(p);
		String tmp = String.valueOf(p.id);
		list.put(tmp, p);
		System.out.println("");

	}


	public void listProduct() {
		for (Map.Entry<String, Product> entry : list.entrySet()) {
			System.out.printf("Product id: %s ", entry.getKey());
			showProduct(entry.getValue());	
		}
		System.out.println("");
	}

	public void addStock(Scanner sc) {
		listProduct();
		System.out.printf("Product ID: ");
		String key = sc.next();
		try {
			Product p = list.get(key);
			System.out.printf("Add in stock: ");
			p.quantity += sc.nextInt();
			list.put(key, p);
			showProduct(p);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void removeStock(Scanner sc) {
		listProduct();
		System.out.printf("Product ID: ");
		String key = sc.next();
		try {
			Product p = list.get(key);
			System.out.printf("Remove in stock: ");
			p.quantity -= sc.nextInt();
			list.put(key, p);
			showProduct(p);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void removeProduct(Scanner sc) {
		listProduct();
		System.out.printf("Product ID: ");
		String key = sc.next();
		try {
			list.remove(key);
			listProduct();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void showProduct(Product p) {
		System.out.printf("Product data: %s, $ %.2f, %d units, total: $ %.2f \n", p.name, p.price, p.quantity,
				p.price * p.quantity);
	}
}
