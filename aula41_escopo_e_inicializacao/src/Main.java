
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		example1_inicializacao();
		example1_escopo();
	}
	
	public static void example1_inicializacao() {
		// TODO Auto-generated method stub
		double price = 400.00;
		// Variável deve ser inicializada se não, dá erro
		// ex: 
		// nova_variavel = 1
	}
	
	private static void example1_escopo() {
		// TODO Auto-generated method stub
		double price = 400.0;
		 if (price < 200.0) {
			 double discount = price * 0.1;
		 }
		 // Variável discount só pode ser usada dentro da condicional
		 // fora dela irá gerar um erro
		 // System.out.println(discount);
	}

}
