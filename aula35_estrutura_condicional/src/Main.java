import java.util.Calendar;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		exemplo1();
	}
	
	public static void exemplo1() {
		Calendar now = Calendar.getInstance();
		int hora = now.get(Calendar.HOUR_OF_DAY);
		if(hora < 6) {
			System.out.printf("Boa madrugada! São %d horas da madruga", hora);
		}else if(hora < 12 ) {
			System.out.printf("Bom dia! São %d horas da manhã", hora);
		}else if(hora < 19) {
			System.out.printf("Boa tarde! São %d horas da tarde", hora);
		}else if(hora < 24 ) {
			System.out.printf("Boa noite! São %d horas da noite", hora);
		}

	}

}
