package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Products;

public class Program {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String oper = "1";
		Products products = new Products();
		Locale.setDefault(Locale.US);
		do {
			System.out.println("What do you want? ");
			System.out.println("1 - New product");
			System.out.println("2 - List product");
			System.out.println("3 - Add product stock");
			System.out.println("4 - Remove product stock");
			System.out.println("5 - Remove product");
			System.out.println("0 - Exit");
			System.out.printf("Operation: ");
			oper = sc.next();
			System.out.printf("\n");
			switch (oper) {
			case "1":
				products.newProduct(sc);
				break;
			case "2":
				products.listProduct();
				break;
			case "3":
				products.addStock(sc);
				break;
			case "4":
				products.removeStock(sc);
				break;
			case "5":
				products.removeProduct(sc);
				break;
			case "0":
				System.out.println("Thank you!");
				break;
			default:
				break;
			}
		} while (!(oper.equals("0")));

	}

}
