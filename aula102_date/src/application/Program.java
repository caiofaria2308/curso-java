package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.TimeZone;

public class Program {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		example1();
	}
	
	private static void example1() throws ParseException {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat sdf3 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		sdf3.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		Date y1 = sdf1.parse("25/06/2018");
		Date y2 = sdf2.parse("25/06/2018 12:21:47");
		System.out.println(y1);
		System.out.println(sdf1.format(y1));
		System.out.println("");
		System.out.println(y2);
		System.out.println(sdf2.format(y2));
		System.out.println("");
		
		Date now = new Date();
		Date now_mili = new Date(System.currentTimeMillis());
		System.out.println(now);
		System.out.println(sdf2.format(now));
		System.out.println("");
		System.out.println(now_mili);
		System.out.println(sdf2.format(now_mili));
		System.out.println("");
		
		Date x3 = new Date(0L);
		System.out.println(x3);
		System.out.println(sdf2.format(x3));
		System.out.println("");
		Date x4 = new Date(1000L * 60L * 60L* 5L);
		System.out.println(x4);
		System.out.println(sdf2.format(x4));
		System.out.println("");
		
		Date y3 = Date.from(Instant.parse("2018-06-25T15:42:07Z"));
		System.out.println(y3);
		System.out.println(sdf2.format(y3));
		System.out.println("");
		
		System.out.println("SDF3");
		System.out.println(y1);
		System.out.println(sdf3.format(y1));
		System.out.println("");
		System.out.println(y2);
		System.out.println(sdf3.format(y2));
		System.out.println("");
		System.out.println(now);
		System.out.println(sdf3.format(now));
		System.out.println("");
		System.out.println(now_mili);
		System.out.println(sdf3.format(now_mili));
		System.out.println("");
		System.out.println(x3);
		System.out.println(sdf3.format(x3));
		System.out.println("");
		System.out.println(x4);
		System.out.println(sdf3.format(x4));
		System.out.println("");
		System.out.println(y3);
		System.out.println(sdf3.format(y3));
		System.out.println("");
		

	}

}
