package application;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Variáveis do tipo classe, são ponteiros para o local que 
		 * os dados estão sendo armazenados
		 */
		
		/*
		 * Tipos primitivos são tipos valor, os valores são caixas na memória em vez de ponteiros
		 * dentro da variável realmente existe o valor, ao contrario da variável do tipo classe
		 * que aponta para o local que o valor está armazenado
		 */
//		int p;
//		ocorrerá erro pois variável é nula
//		System.out.println(p);
		
		/*
		 * Quando iniciamos qualquer tipo estruturado, classe ou array, são atribuidos os 
		 * valores padrões, exemplo, números receberão o número 0
		 */

	}

}
