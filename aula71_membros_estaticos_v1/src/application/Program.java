package application;

import java.util.Locale;
import java.util.Scanner;

public class Program {
	public static final double PI = 3.14159;
	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter radius: ");
		double radius = sc.nextDouble();
		double c = circumference(radius);
		double v = volume(radius);
		System.out.printf("Circumference: %.2f\nVolume: %.2f\nPI value: %.2f", c, v, PI);
		sc.close();

	}
	
	public static double circumference(double radius) {
		// TODO Auto-generated method stub
		return 2.0 * PI * radius;

	}
	
	public static double volume(double radius) {
		// TODO Auto-generated method stub
		return 4.0 * PI * Math.pow(radius, 3) / 3; 

	}

}
