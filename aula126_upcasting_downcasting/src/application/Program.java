package application;

import entities.Account;
import entities.BusinessAccount;
import entities.SavingsAccount;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Upcasting -> casting da subclasse para superclasse uso comum: polimofimo
		 */

		/*
		 * Downcasting -> Casting da superclasse para subclasse Uso comum em métodos que
		 * recebem parâmetros genéricos
		 */

		Account acc = new Account(1001, "Alex", 0.0);
		BusinessAccount bacc = new BusinessAccount(1002, "Maria", 0.0, 500.00);

		// UPCASTING
		Account acc1 = bacc;
		Account acc2 = new BusinessAccount(1003, "Bob", 0.0, 300.0);
		Account acc3 = new SavingsAccount(1004, "Ana", 0.0, 0.01);
		/*
		 * Funciona porque BusinessAccount é uma Account
		 */

		//BusinessAccount acc4 = acc2;
		BusinessAccount acc4 = (BusinessAccount) acc2;
		acc4.loan(200.0);
		System.out.println(acc4.getBalance());
		// BusinessAccount acc5 = (BusinessAccount) acc3; BROKEN
		if (acc3 instanceof BusinessAccount) {
			BusinessAccount acc5 = (BusinessAccount) acc3;
			acc5.loan(200.0);
			System.out.println("Loan");
		} else if (acc3 instanceof SavingsAccount) {
			SavingsAccount acc5 = (SavingsAccount) acc3;
			acc5.updateBalance();
			System.out.println("Update");
		}
	}

}
