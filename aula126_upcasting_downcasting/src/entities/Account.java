package entities;

public class Account {	
		
	private Integer number;
	private String holder;
	protected Double balance;
	
	public Account() {
	}

	/**
	 * @param number
	 * @param holder
	 * @param balance
	 */
	public Account(Integer number, String holder, Double balance) {
		super();
		this.number = number;
		this.holder = holder;
		this.balance = balance;
	}

	/**
	 * @return the number
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * @return the holder
	 */
	public String getHolder() {
		return holder;
	}

	/**
	 * @param holder the holder to set
	 */
	public void setHolder(String holder) {
		this.holder = holder;
	}

	/**
	 * @return the balance
	 */
	public Double getBalance() {
		return balance;
	}
	
	public void withdraw(Double amount) {
		this.balance -= amount;
	}
	
	public void deposit(Double amount) {
		this.balance += amount;
	}

}
