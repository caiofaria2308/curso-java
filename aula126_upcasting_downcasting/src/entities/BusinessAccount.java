/**
 * 
 */
package entities;

/**
 * @author cnogueira
 *
 */
public class BusinessAccount extends Account {

	/**
	 * 
	 */
	private Double loanlimit;

	public BusinessAccount() {
		// TODO Auto-generated constructor stub
		super();
	}

	/**
	 * @param number
	 * @param holder
	 * @param balance
	 * @param loanlimit
	 */
	public BusinessAccount(Integer number, String holder, Double balance, Double loanlimit) {
		super(number, holder, balance);
		this.loanlimit = loanlimit;
	}

	/**
	 * @return the loanlimit
	 */
	public Double getLoanlimit() {
		return loanlimit;
	}

	/**
	 * @param loanlimit the loanlimit to set
	 */
	public void setLoanlimit(Double loanlimit) {
		this.loanlimit = loanlimit;
	}
	
	public void loan(Double amount) {
		if (amount <= this.loanlimit) {
			this.balance += amount - 10.0;
		}
	}

}
