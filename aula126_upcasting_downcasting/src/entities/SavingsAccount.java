/**
 * 
 */
package entities;

/**
 * @author cnogueira
 *
 */
public class SavingsAccount extends Account {

	private Double interestRate;

	public SavingsAccount() {
		super();
	}

	/**
	 * @param number
	 * @param holder
	 * @param balance
	 * @param interestRate
	 */
	public SavingsAccount(Integer number, String holder, Double balance, Double interestRate) {
		super(number, holder, balance);
		this.interestRate = interestRate;
	}

	/**
	 * @return the interestRate
	 */
	public Double getInterestRate() {
		return interestRate;
	}

	/**
	 * @param interestRate the interestRate to set
	 */
	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	/*
	 * update balance based on interest rate
	 */
	public void updateBalance() {
		this.balance += this.balance * this.interestRate;
	}

}
