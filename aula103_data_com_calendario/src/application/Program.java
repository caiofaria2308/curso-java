package application;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		example1();

	}
	
	private static void example1() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date d = Date.from(Instant.parse("2018-06-25T15:42:07Z"));
		System.out.println(sdf.format(d));
		Calendar ca1 = Calendar.getInstance();
		ca1.setTime(d);
		ca1.add(Calendar.HOUR_OF_DAY, 4);
		d = ca1.getTime();
		System.out.println(sdf.format(d));
		
		int minutes = ca1.get(Calendar.MINUTE);
		System.out.println(minutes);
		int month = 1 + ca1.get(Calendar.MONTH);
		System.out.println(month);
		

	}

}
