import java.util.Locale;

public class Main {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		double x = 3.0;
		double y = 4.0;
		double z = -5.0;
		
		example1_sqrt(x, y);
		example1_pow(x, y);
		example1_abs(y, z);

	}
	
	public static void example1_sqrt(double x, double y) {
		 System.out.println("Exemplo 1: Raiz Quadrada");
		 double A, B, C;
		 A = Math.sqrt(x);
		 B = Math.sqrt(y);
		 C = Math.sqrt(25.0);
		 System.out.printf("Raiz quadrada de %.2f = %.2f\n"
		 		+ "Raiz quadrada de %.2f = %.2f\n"
		 		+ "Raiz quadrada de %.2f = %.2f\n",
		 		x, A,
		 		y, B,
		 		25.0, C);
		 
	}
	
	public static void example1_pow(double x, double y) {
		System.out.println("Exemplo 1: Exponenciação ");
		double A = Math.pow(x, y);
		double B = Math.pow(x, 2.0);
		double C = Math.pow(5.0, 2.0);
		System.out.printf("%.2f elevado ao %.2f = %.2f\n"
				+ "%.2f elevado ao quadrado = %.2f\n"
				+ "%.2f elevado ao quadrado = %.2f\n",
				x, y, A,
				x, B,
				5.0, C);
	}
	
	public static void example1_abs(double y, double z) {
		System.out.println("Exemplo 1: valor absoluto");
		double A = Math.abs(y);
		double B = Math.abs(z);
		
		System.out.printf("Valor absoluto de: %.2f = %.2f\n"
				+ "Valor absoluto de: %.2f = %.2f\n",
				y, A,
				z, B);
	}
	
	

}
