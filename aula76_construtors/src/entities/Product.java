package entities;


public class Product {
	public int id;
	public String name;
	public double price;
	public int quantity;
	
	public Product(int id, String name, double price, int quantity) {
		this.name = name;
		this.price = price;
		this.id = id;
		this.quantity = quantity;
				
		
	}
}
